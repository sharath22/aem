<%--
  Training Title component from Adobe Trainer.
  Title navigation component for training project
--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %>
<%@taglib prefix="customtaglib" uri="/apps/training/tlds/trainingTagLib.tld" %>
<%
%>
<customtaglib:tt firstName="${param.a}" lastName="${param.b}" contactNo="${param.c}"/>


<cq:includeClientLib categories="training.title"/>
<h2><%= properties.get("title", currentPage.getTitle()) %></h2>
<h3><%= properties.get("address", "") %></h3>

<h2><%= currentStyle.get("title_design","") %></h2>
<h3><%= currentStyle.get("address_design","") %></h3>

<div>
<p id="firstContainer" class="firstContainer">Click Button</p>
</div>
<input type="button" id="sample_button" onclick="getNameFirst()" value="test">