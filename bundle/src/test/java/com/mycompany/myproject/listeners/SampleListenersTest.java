package com.mycompany.myproject.listeners;

import static org.junit.Assert.assertTrue;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import mockit.Cascading;
import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;

import org.apache.sling.jcr.api.SlingRepository;
import org.junit.Test;
import org.osgi.service.component.ComponentContext;

public class SampleListenersTest {	
	
	@Tested
	SampleListeners sampleListener;
	
	@Mocked
	private SlingRepository repository;

	@Mocked
	ComponentContext context;
	
	@Cascading
	Session adminSession;
	
	@Mocked
	ObservationManager observationMgr;
	
	@Mocked
	EventIterator eventIterator;
	
	
	@Test
	public final void testActivate() throws RepositoryException {
		
		new NonStrictExpectations(){
			{
				repository.loginAdministrative(null);
				returns(adminSession);

				adminSession.getWorkspace().getObservationManager();
				returns(observationMgr);				
			}
		};
		assertTrue("message", true);
		Deencapsulation.setField(sampleListener, "repository", repository);
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		sampleListener.activate(context);
	}
	
	

	@Test
	public final void testActivate_Exception() {
		try {
			new NonStrictExpectations() {
				{
					repository.loginAdministrative(null);
					result = new RepositoryException();
				}
			};
		} catch (RepositoryException e) {
		}
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		Deencapsulation.setField(sampleListener, "repository", repository);
		sampleListener.activate(context);
	}

	@Test
	public final void testActivate_Null() {
		try {
			new NonStrictExpectations() {
				{
					repository.loginAdministrative(null);
					returns(adminSession);

					adminSession.getWorkspace().getObservationManager();
					returns(observationMgr);

					result = new NullPointerException();
				}
			};
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		Deencapsulation.setField(sampleListener, "repository", repository);
		sampleListener.activate(context);
	}

	
	@Test
	public final void testOnEvent() {
		new NonStrictExpectations() {
			{
				eventIterator.hasNext();
				returns(true);
			}
		};
		sampleListener.onEvent(eventIterator);
	}

	@Test
	public final void testOnEventFalse() {
		new NonStrictExpectations() {
			{
				eventIterator.hasNext();
				returns(false);
			}
		};
		sampleListener.onEvent(eventIterator);
	}

	@Test
	public final void testDeactivate() throws RepositoryException {
		Deencapsulation.setField(sampleListener, "observationMgr", observationMgr);
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		sampleListener.deactivate(context);
	}

	@Test
	public final void testDeactivate_null() throws RepositoryException {
		Deencapsulation.setField(sampleListener, "observationMgr", null);
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		sampleListener.deactivate(context);
	}

	@Test
	public final void testDeactivate_null_null() throws RepositoryException {
		Deencapsulation.setField(sampleListener, "observationMgr", null);
		Deencapsulation.setField(sampleListener, "adminSession", null);
		sampleListener.deactivate(context);
	}

	@Test
	public final void testDeactivate_Exception() {
		try {
			new NonStrictExpectations() {
				{
					observationMgr.removeEventListener((EventListener) any);
					result = new RepositoryException();
				}
			};
		} catch (RepositoryException e) {
		}
		Deencapsulation.setField(sampleListener, "observationMgr", observationMgr);
		Deencapsulation.setField(sampleListener, "adminSession", adminSession);
		sampleListener.deactivate(context);
	}

	@Test
	public final void testDeactivate_Finally1() {
		try {
			new NonStrictExpectations() {
				{
					observationMgr.removeEventListener((EventListener) any);
					result = new RepositoryException();
				}
			};
		} catch (RepositoryException e) {
		}
		Deencapsulation.setField(sampleListener, "observationMgr", observationMgr);
		Deencapsulation.setField(sampleListener, "adminSession", null);
		sampleListener.deactivate(context);
	}
}
