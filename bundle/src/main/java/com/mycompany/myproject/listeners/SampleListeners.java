package com.mycompany.myproject.listeners;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Component(immediate = true, label = "Sample Listener", metatype = false)
@Properties({ @Property(name = Constants.SERVICE_VENDOR, value = "My Company"), @Property(name = Constants.SERVICE_DESCRIPTION, value="Listener Description") })
public class SampleListeners implements EventListener {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SampleListeners.class);
	/** The admin session. */
	private Session adminSession;
	/** The observation manager. */
	private ObservationManager observationMgr;
	/** The repository. */
	@Reference
	private SlingRepository repository;	
	
	@Activate
	protected void activate(ComponentContext context) {
		try {
			LOG.info("Sample Listener code starts - activate method");	
			adminSession = repository.loginAdministrative(null);
			observationMgr = adminSession.getWorkspace().getObservationManager();
			final String[] types = { "nt:unstructured","sling:Folder","cq:Page","cq:PageContent","sling:OrderedFolder" };
			final String path = "/content/Testing/en/testpage30";
			observationMgr.addEventListener(this, 
					Event.NODE_REMOVED | Event.NODE_MOVED | Event.NODE_ADDED | Event.PROPERTY_ADDED | Event.PROPERTY_CHANGED | Event.PROPERTY_REMOVED, 
					path, 
					true, 																																												
					null, 
					types, 
					false);
			LOG.info("Sample Listener code ends");
		} catch (RepositoryException e) {
			LOG.error("unable to register session", e);
		} catch (Exception e) {
			LOG.error("Exception:", e);
		} 
	}

	/**
	 * Deactivate.
	 */
	@Deactivate
	protected void deactivate(ComponentContext componentContext) {		
		try {
			if (observationMgr != null) {
				observationMgr.removeEventListener(this);
				LOG.info("Removed JCR event listener");
			}
		} catch (RepositoryException re) {
			LOG.error("Error while removing the event listener", re);
		} finally {
			if (adminSession != null) {
				adminSession.logout();
				adminSession = null;
			}
		}
		
	}
	
	public void onEvent(EventIterator eventIterator) {
		LOG.info("Sample Listener: onEvent method");
		if (eventIterator.hasNext()) {
			//Business Logic
		}
	}
}
