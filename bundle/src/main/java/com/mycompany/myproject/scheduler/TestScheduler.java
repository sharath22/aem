package com.mycompany.myproject.scheduler;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;

import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, label = "Training Scheduler", description = "Training Scheduler Desc", metatype = true)
@Service
@Properties({
	@Property(name = Constants.SERVICE_DESCRIPTION, value = "Training Scheduler Service"),
	@Property(name = Constants.SERVICE_VENDOR, value = "Training"),
	@Property(name = "scheduler.concurrent", boolValue = false),
	@Property(name = "scheduler.expression", value = "0/5 * * 1/1 * ? *", description = "Scheduler Cron Expression. Follow the link http://www.docjar.com/docs/api/org/quartz/CronExpression.html for more details.") })
public class TestScheduler implements Runnable  {
	//seconds, minutes, hour, day of the month, month, day of the week
	private static final Logger LOG = LoggerFactory.getLogger(TestScheduler.class);


	public void run() {

		try {
			LOG.info("Hello");
		} catch (Exception ex) {
			LOG.error("Exception:: " + ex);
		}
	}


	
}

