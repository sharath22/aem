package com.mycompany.myproject.constants;

public class GlobalConstants {

	
	/** The Constant UTF_8. */
	public static final String UTF_8 = "UTF-8";
	
	public static final String CQ_TAGS = "cq:tags";
	
	public static final String METADATA = "metadata";
	
	public static final String PATH = "path";
	
	public static final String APPLICATION_JSON = "application/json";
	
	public static final String FORCE = "force";
	
	public static final String TRUE = "true";
	
	public static final String ELIGIBLE_VEHICLE_JSON_FILE_NAME = "retrieveEligibleVehicles.json";

	public static final String ELIGIBLE_VEHICLE_MAKE_JSON_FILE_NAME = "retrieveEligibleVehiclesMake.json";
	
	public static final String ELIGIBLE_VEHICLE_MODEL_JSON_FILE_NAME = "retrieveEligibleVehiclesModel.json";
	
	public static final String ELIGIBLE_VEHICLE_YEAR_JSON_FILE_NAME = "retrieveEligibleVehiclesYear.json";
	
	public static final String ELIGIBLE_VEHICLE_BODY_JSON_FILE_NAME = "retrieveEligibleVehiclesBody.json";
	
	public static final String ELIGIBLE_VEHICLE_CARINFO_JSON_FILE_NAME = "retrieveEligibleVehiclesCarInfo.json";
	
	public static final String NT_FILE = "nt:file";
	
	public static final String NT_RESOURCE = "nt:resource";
	
	public static final String NT_FOLDER = "nt:folder";
	
	public static final String JCR_MIME_TYPE = "jcr:mimeType";
	
	public static final String EMPTY_JSON = "{}";	
	
	public static final String OFF = "off";
	
	public static final String ON = "on";
	
	public static final String HTTP_PROXYHOST = "http.proxyHost";
	
	public static final String HTTP_PROXYPORT = "http.proxyPort";	
	
	public static final String GETMAKES_VEHICLE_JSON_FILE_NAME = "carinfo_makes.json";
	
	public static final String MAKE_VEHICLE_JSON_FILE_NAME = "makeid_";
	
	public static final String GETMODELS_VEHICLE_JSON_FILE_NAME = "modelid_";
	
	public static final String GETYEARS_VEHICLE_JSON_FILE_NAME = "year_";
	
	public static final String GETBODYTYPES_VEHICLE_JSON_FILE_NAME = "vehicleid_";
		
	public static final String FORWARD_SLASH = "/";
	
	public static final String JCR_CONTENT = "jcr:content";
	
	public static final String JCR_ENCODING = "jcr:encoding";
	
	public static final String JSON_EXTENSION = ".json";
	
	public static final String JCR_DATA = "jcr:data";
	
	public static final String MAKE_ID= "MakeId";
	
	public static final String MODEL_ID= "ModelId";
	
	public static final String YEAR_ID= "Year";
	
	public static final String VEHICLE_ID= "VehicleId";
	
	public static final String NT_UNSTRUCTURED = "nt:unstructured";
	
}
