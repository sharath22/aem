package com.mycompany.myproject.workflows;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;

@Component
@Service
@Properties({
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "Sample Workflow Description"),
		@Property(name = Constants.SERVICE_VENDOR, value = "Training"),
		@Property(name = "process.label", value = "Sample Workflow") })

public class SampleWorkflow implements WorkflowProcess {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SampleWorkflow.class);

		
	public void execute(final WorkItem item, final WorkflowSession wfSession,
			final MetaDataMap args) throws WorkflowException {
		
		try {
			WorkflowData workflowData = item.getWorkflowData();
			String payloadPath = workflowData.getPayload().toString();
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("payloadPath::" + payloadPath);
			}
			
		} catch (Exception e) {
			LOGGER.error("CacheInvalidationWorkflow::Exception occured::Exception is::"
					+ e);
		}		

	}
}