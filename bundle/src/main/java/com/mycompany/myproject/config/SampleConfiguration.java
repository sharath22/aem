package com.mycompany.myproject.config;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class SampleConfiguration used to. configure application level
 * properties in Felix console.
 * 
 *  
 */
@Component(immediate = true, label = "Sample Test Configuration", description = "Sample Test Configuration", metatype = true)
@Properties({
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Sample-Config"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Training") })
@Service(SampleConfiguration.class)
public class SampleConfiguration {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory
            .getLogger(SampleConfiguration.class);

    /** The Constant CONTENT_PATH. */
    @Property(label = "Content path", value = "/content/cdc", description = "Configure application content rootpath")
    public static final String CONTENT_PATH = "cdc.content.path";
    
    /** The Constant TAGS_PATH. */
    @Property(label = "Application Categories Tags Root Path", value = "/etc/tags/cdc", description = "Configure tags root path")
    public static final String TAGS_PATH = "cdc.tags.root.path";

    /** The Constant DAM_PATH. */
    @Property(label = "DAM Path", value = "/content/dam/cdc/pdf/", description = "Configure Dam Path for PDF upload")
    public static final String DAM_PATH_FOR_PDF_UPLOAD = "cdc.dam.path";
    
    /** The properties. */
    @SuppressWarnings("rawtypes")
    private static Dictionary properties = null;

    /**
     * Activate.
     * 
     * @param componentContext
     *            the component context
     */
    @Activate
    protected final void activate(final ComponentContext componentContext) {
        LOG.info("CDC Configuration Service:: Activated method called");
        try {
            properties = componentContext.getProperties();
           
        } catch (Exception e) {
            LOG.error("CDC Configuration ::"
                    + "Error occured in activate method");
        }
    }


    /**
     * Deactivate.
     * 
     * @param context
     *            the context
     */
    @Deactivate
    protected final void deactivate(final ComponentContext context) {
        LOG.info("CDC Configuration  : deactivating service");
    }
    
    public static Object getPropertyValue(final String propertyName) {
		LOG.info("Priceless Configuration : getPropertyValue ::" + propertyName);
		return properties.get(propertyName);

	}

}
