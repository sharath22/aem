package com.mycompany.myproject.servlet;

import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(immediate=true, metatype=true, enabled=true )
@Service(Servlet.class)
@Properties({
	@Property(name="sling.servlet.paths", value="/bin/samplehit"),
	@Property(name="sling.servlet.methods", value="GET")
})

public class SampleServlet extends SlingAllMethodsServlet{
     
    /**
	 * 
	 */
	private static final long serialVersionUID = 4552754396434451831L;
	
	/** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(SampleServlet.class);   

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {        
            
        try {            
        	LOG.info("Starting: Hello Sample Servlet"); 
        	for(int i=0; i<=100; i++){
        		LOG.info("Iteration #"+i+"  :Hello Sample Servlet"); 
        	}
        	LOG.info("Ending: Hello Sample Servlet");          
        } catch (Exception e) {
            LOG.error("Exception: " + e);
        }
    }       
    
    protected void activate(ComponentContext context)
    {        
        try {
           LOG.info("activate sample servlet");            
        }
        catch(Exception e) {
            LOG.error("Exception was thrown in activation of CDN service : ", e);
        }
    }
    
    protected void deactivate(ComponentContext ctx) throws Exception {
    	LOG.info("deactivate sample servlet");
    }	
}
/*testing */
